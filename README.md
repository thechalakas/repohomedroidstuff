# repohomedroidstuff

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to android development.

**here are the repos**

I have written some code on android. I use this as sample code for android projects for clients. I also use it for my training sessions. 

---

1. [https://bitbucket.org/thechalakas/actionbar]

2. [https://bitbucket.org/thechalakas/actionbar2]

3. [https://bitbucket.org/thechalakas/activitylifecycle]

4. [https://bitbucket.org/thechalakas/autocompletetextview]

5. [https://bitbucket.org/thechalakas/cardview]

6. [https://bitbucket.org/thechalakas/cursoradapter]

7. [https://bitbucket.org/thechalakas/datepicker]

8. [https://bitbucket.org/thechalakas/deviceconfiguration]

9. [https://bitbucket.org/thechalakas/drawerlayout]

10. [https://bitbucket.org/thechalakas/dropboxdemo]

11. [https://bitbucket.org/thechalakas/dropboxdemo2]

12. [https://bitbucket.org/thechalakas/eureka_module1]

13. [https://bitbucket.org/thechalakas/externalappentry]

14. [https://bitbucket.org/thechalakas/facebooklogin]

15. [https://bitbucket.org/thechalakas/fragments]

16. [https://bitbucket.org/thechalakas/frameanimation]

17. [https://bitbucket.org/thechalakas/framelayout]

18. [https://bitbucket.org/thechalakas/framents2]

19. [https://bitbucket.org/thechalakas/googlelogin]

20. [https://bitbucket.org/thechalakas/googlemapdemo]

21. [https://bitbucket.org/thechalakas/gridview]

22. [https://bitbucket.org/thechalakas/hello_world_1]

23. [https://bitbucket.org/thechalakas/httpurlconnection]

24. [https://bitbucket.org/thechalakas/intents]

25. [https://bitbucket.org/thechalakas/intentservice]

26. [https://bitbucket.org/thechalakas/intentservice2]

27. [https://bitbucket.org/thechalakas/intentservice3]

28. [https://bitbucket.org/thechalakas/layoutpractice]

29. [https://bitbucket.org/thechalakas/linearlayout]

30. [https://bitbucket.org/thechalakas/listviewdemo1]

31. [https://bitbucket.org/thechalakas/listviewdemo2]

32. [https://bitbucket.org/thechalakas/localization]

33. [https://bitbucket.org/thechalakas/mapdemo]

34. [https://bitbucket.org/thechalakas/mediaplayer]

35. [https://bitbucket.org/thechalakas/navigationdrawer1]

36. [https://bitbucket.org/thechalakas/notifications]

37. [https://bitbucket.org/thechalakas/palette]

38. [https://bitbucket.org/thechalakas/persistentstate]

39. [https://bitbucket.org/thechalakas/picasso]

40. [https://bitbucket.org/thechalakas/propertyanimation]

41. [https://bitbucket.org/thechalakas/publishingtostore1]

42. [https://bitbucket.org/thechalakas/relativelayout]

43. [https://bitbucket.org/thechalakas/retrofit]

44. [https://bitbucket.org/thechalakas/sqliteopenhelper]

45. [https://bitbucket.org/thechalakas/storeexternal]

46. [https://bitbucket.org/thechalakas/storeinternal]

47. [https://bitbucket.org/thechalakas/tablelayout]

48. [https://bitbucket.org/thechalakas/timepicker]

49. [https://bitbucket.org/thechalakas/tweenanimation]

50. [https://bitbucket.org/thechalakas/userlocation]

51. [https://bitbucket.org/thechalakas/webapidemo]

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 